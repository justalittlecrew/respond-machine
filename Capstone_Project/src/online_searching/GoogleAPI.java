package online_searching;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.*;

public class GoogleAPI {

	public static String search(String searchString) throws Exception {
		String strKey = "AIzaSyAQVbMUFTXH2bzkBCptdAf7pkR4GgJw1YA";
		// String strKey="AIzaSyBzc6GowscSrGTUuwNU6TG1qJ2sBX3RU-c"; //crew

		String strID = "009492754123209063812:_aqyokaljei";
		// String strID= "013074955856126969430:qt-xw4fop88"; // /crew
		int num = 10;
		// String strQuery="self+driving+car";
		String strQuery = searchString.replace(" ", "+");
		String str = "https://www.googleapis.com/customsearch/v1?key=" + strKey
				+ "&cx=" + strID + "&num=" + num + "&start=1&q=" + strQuery;
		System.out.print(str);
		URL myURL = new URL(str);
		HttpURLConnection conn = (HttpURLConnection) myURL.openConnection();
		InputStream is = conn.getInputStream();
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		StringBuffer buffer = new StringBuffer();
		String line = null;

		while (null != (line = br.readLine())) {
			buffer.append(line + "\n");
		}
		br.close();
		isr.close();
		is.close();

		// System.out.print(buffer);
		return parseJson(buffer.toString());

	}

	public static String parseJson(String jsonString) throws JSONException {

		JSONObject obj = new JSONObject(jsonString);
		String titleString = "";
		String snippetString = "";

		JSONArray arr;

		try {
			arr = obj.getJSONArray("items");
		} catch (Exception ex) {
			return "";
		}

		for (int i = 0; i < arr.length(); i++) {
			String title = arr.getJSONObject(i).getString("title");
			titleString += " " + title;

			String snippet = arr.getJSONObject(i).getString("snippet");
			snippetString += " " + snippet;

		}
		// System.out.println(titleString);

		System.out.println();

		// System.out.println(snippetString);

		return titleString + snippetString;
	}
}
