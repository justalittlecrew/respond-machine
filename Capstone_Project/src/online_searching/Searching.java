package online_searching;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import mylibrary.MyLib_General;
import core.Application;

public class Searching {

//	public static int ML_maxAddCount=3;
//	public static boolean ML = false ;
	
	public static String search(String inputQuery) {
		
		//Application.onlineQueryExpansionCount +=1;
		long tStart = System.currentTimeMillis();


		
		
		inputQuery=inputQuery.toLowerCase();
		
		//if (ML) return machineLearning(inputQuery) ;
		
		MaxDuplicateWordCount.createStopWord();
		try {
			int maxAddCount=3;
			int addCounter=0;
			String [] topAddKeyword = new String[maxAddCount];
			
			MaxDuplicateWordCount.mdc = new MaxDuplicateWordCount();
			MaxDuplicateWordCount.wordMap = new HashMap<String, Integer>();
			

			String result1=GoogleAPI.search(inputQuery);
			MaxDuplicateWordCount.wordMap = MaxDuplicateWordCount.mdc.getWordCount(result1, 1);
			
			
			String result2=BingAPI.search(inputQuery);
			MaxDuplicateWordCount.wordMap = MaxDuplicateWordCount.mdc.getWordCount(result2, 1);

			
	        List<Entry<String, Integer>> list1 = MaxDuplicateWordCount.mdc.sortByValue(MaxDuplicateWordCount.wordMap);

	        
	        List<Entry<String, Integer>> list2 = MaxDuplicateWordCount.mdc.sortByValue(MaxDuplicateWordCount.wordMap);
	        for(Map.Entry<String, Integer> entry:list2){
	        	if (entry.getKey().contains("twitter"))continue;
	        	if (entry.getKey().contains("tweet"))continue;
	        	if (entry.getKey().contains("tweets"))continue;
	        	
	        	if (inputQuery.toLowerCase().contains(entry.getKey())) continue;
	        	if (!entry.getKey().matches("\\A\\p{ASCII}*\\z")) continue;
	        	if (entry.getKey().matches("[0-9]+")) continue;
	        	
	        	topAddKeyword[addCounter] = entry.getKey();
	        	addCounter+=1;
	        	if (addCounter>=maxAddCount) break;
	          //  System.out.println(entry.getKey()+" ==== "+entry.getValue());
	        }  

	        
	        System.out.println(Arrays.toString(topAddKeyword));
	        
	        String returnString = inputQuery;
	        String recordString = "";
	        for (int i=0;i<topAddKeyword.length;i++){
	        	returnString += " " + topAddKeyword[i];
	        	recordString+= topAddKeyword[i]+" ";
	        }
	       // recordResultString(Application.trainingPost + "\t"+ recordString.trim());
	        
	        
			long tEnd = System.currentTimeMillis();
			long tDelta = tEnd - tStart;
			double elapsedSeconds = tDelta / 1000.0;
			//System.out.print("online query time:" + elapsedSeconds);
			
	        return returnString;
	        
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return inputQuery;
		}


	}
	/**
	static void recordResultString(String line){
		
		//Application.experiment.add(line);

	}
	
static String machineLearning(String tmp){
	BufferedReader reader1;
	String resultString =tmp;
	String line1;
	
		try {
			reader1 = new BufferedReader(new FileReader(System.getProperty("user.dir") + "\\experiment\\Query_expansion"));
			while ((line1 = reader1.readLine()) != null) {
				String postId = MyLib_General.getFieldData(line1, 1, "\t");
				if (postId.equals(Application.trainingPost)){
					
					String [] expandList = MyLib_General.getFieldData(line1, 2, "\t").split(" ");
					System.out.println(Arrays.toString(expandList));
					
					for(int i=0;i<Application.onlineQueryExpansionCount;i++){
						resultString+= " " + expandList[i];
					}
					System.out.println("gggg expand");
					return resultString.trim();
				}
			}

		
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return tmp;
}
**/
}
