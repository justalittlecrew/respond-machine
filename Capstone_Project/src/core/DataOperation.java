package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;

import mylibrary.MyLib_General;

public class DataOperation {

	public static String getCommentFromId(String id, int language) { // 0= eng, 1=chin
		BufferedReader reader2;
		try {
			if (language ==0)
			reader2 = new BufferedReader(
					new FileReader(Application.path_repos_cmnt));
			else
				reader2 = new BufferedReader(
						new FileReader(Application.path_repos_cmnt_cn));
			
					//new FileReader(System.getProperty("user.dir") + "\\filtered_cmnt"));
			String line="";
			while ((line = reader2.readLine()) != null) {
				if (MyLib_General.getFieldData(line, 1, "\t").equals(id)){
					reader2.close();
					return MyLib_General.getFieldData(line, 2, "\t");
				}
			}
			reader2.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
		
	}

	public static String getPostFromId(String id) {
		BufferedReader reader2;
		try {
			reader2 = new BufferedReader(
					new FileReader(Application.path_repos_post));
			String line="";
			while ((line = reader2.readLine()) != null) {
				if (MyLib_General.getFieldData(line, 1, "\t").equals(id)){
					reader2.close();
					return MyLib_General.getFieldData(line, 2, "\t");
				}
			}
			reader2.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
		
	}

	public static ArrayList getCmntsFromCmntIds(ArrayList cmntIdList) {
		ArrayList resultList = new ArrayList();
		BufferedReader reader2;
		try {
			reader2 = new BufferedReader(
					new FileReader(Application.path_repos_cmnt));
			String line="";
			while ((line = reader2.readLine()) != null) {
				String cmntId = MyLib_General.getFieldData(line, 1, "\t");
				if (cmntIdList.indexOf(cmntId) !=-1  && resultList.indexOf(line)==-1 ){
					resultList.add(line);
				
				}
			}
			reader2.close();
			return resultList;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ArrayList();
		
	}

	public static ArrayList getCmntWithWeightIdListFromCmntIdList(ArrayList CmntIdList) {
		BufferedReader reader2;
		ArrayList returnList  = new ArrayList();
		//System.out.println(CmntIdList.toString());
		try {
			reader2 = new BufferedReader(
		//			new FileReader(System.getProperty("user.dir") + "\\repos-id-cmnt-en"));
					new FileReader(Application.path_filtered_cmnt_weighted));
			String line="";
			while ((line = reader2.readLine()) != null) {
				String cmntKey = MyLib_General.getFieldData(line, 1, "\t");
				
				if (CmntIdList.indexOf(cmntKey)!=-1){
					returnList.add(line);
	
				}
			}
			reader2.close();
			return returnList;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ArrayList();
		
	}

	public static ArrayList getCommentIdsFromPostId(String id) {
		ArrayList commentIds = new ArrayList();
		
		BufferedReader reader2;
		try {
			reader2 = new BufferedReader(
					new FileReader(Application.path_index_file));
			String line="";
			while ((line = reader2.readLine()) != null) {
				if (MyLib_General.getFieldData(line, 1, "\t").equals(id)){
					commentIds.add(MyLib_General.getFieldData(line, 2, "\t"));
				}
			}
			reader2.close();
			return commentIds;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return new ArrayList();
		
	}

	static String [] getCommentIdList(String postId){
		String commentIdList[] = new String[30];
		int counter=0;
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(Application.path_index_file));
	
			String line;
	
			while ((line = reader.readLine()) != null) {
	
				if (line.split("\t")[0].equals(postId)){
					commentIdList[counter]=line;
					counter+=1;
					if (counter>=30) break;
							
				}
				// if (counter % 1000 == 0)
				// System.out.println(counter);
			}
	
			reader.close();
	
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		
		return  commentIdList;
	}

	public static String getPostIdFromCmntId(String cmntId){
		
		try {
			BufferedReader reader = new BufferedReader(
					new FileReader(Application.path_index_file));
			String line;
		
			while ((line = reader.readLine()) != null) {
				if (MyLib_General.getFieldData(line, 2, "\t").equals(cmntId)){
					reader.close();
					return MyLib_General.getFieldData(line, 1, "\t");
				}
			}
			reader.close();
	
	
		} catch (Exception e) {
			final StringWriter sw = new StringWriter();
			final PrintWriter pw = new PrintWriter(sw, true);
			e.printStackTrace(pw);
			System.out.println(sw.getBuffer().toString());
			sw.getBuffer().toString();
		}
		return "";
	}

	
}
